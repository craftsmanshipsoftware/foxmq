FoxMQ is a simple messaging application layer protocol for RESTful communication between clients and a server.  It includes publish-subscribe, post-response and broadcast messaging types.  FoxMQ uses key-value pairs where the key is known as a topic and the value is known as the content. The content is a binary blob.  FoxMQ includes User Access Control where connecting clients are assigned a list of different topic prefixes and / or whole topics depending upon their permissions.

![ALT](spec/birds-eye-usage.svg)

* Clients maintain their connection to the server.
* Messages are made of a header, topic and content.
* Specifies two libraries, a client and a server library.

Qt Implementation

* Uses both QSslSockets and QWebSockets in the server.  The Qt client library only has QSslSockets specified.  Webpages / javascript can connect to the QWebSockets.
* The server can operate either or both of the QSslSocket listener and QWebSocket listener on ports of its choosing.
* Server and client authentication can be done using QSslSocket's api

## Action Types

Messages are broken into different types called action types.  These are enumerated in common/foxmq-head.h  Different action types are used for publish-subscribe messages from post-response messages.  The action type is put into the header.  FoxMQ manages building the header in both the client and server libraries.  The user calls high level functions for the messaging they wish to use.

## Messaging

### Publish-Subscribe

* A copy of the latest published message for each topic is stored in the FoxMQ den for future clients to receive if subscribed to.
* Clients can subscribe and unsubscribe to a published topic throughout their connection to the server using the subscribe and unsubscribe action types.
* a client can subscribe to a family of topics.  Subscribing to "user" also will enable receiving topics "user.added", "user.removed", "user.ro.list".  ( user* ).  One could also subscribe to user.r and then just receive "user.removed", "user.ro.list".  A client can subscribe to a family of topics ( user ) and also to a specific topic ( user.removed ) in the same family at the same time.
* The server manages the subscriptions.  When a publish happens, the server determines which clients should receive the topic based on who has subscribed to it.
* Clients can send a change request action type to the server to alter the content of a published topic.  The FoxMQ den library emits a signal / callback for the server to process the change request.  The client sends new content in the change request.  The server then publishes the new content from the client.
* The server can publish topics independent of client change requests.
* Pusblishes have the publish action type.
* Published topics contain the id of the client that made the change request in the message header.  If the server originated the publish without a change request, the server's id is used in the header.  The purpose of this is to know who made the change request.
* The header contains a publish count.  Each time the server publishes a topic, the publish count increases by one.
* Change requests are sent with the latest publish count for that topic.

#### Publish Subscribe Synchronization and UX

It can happen that multiple clients can send a change request for the same published value close in time. (at the same time).  For example a television may be publishing that it is on channel 11.  Client A sends a change request to change the channel to 5 and client B sends a change request to change the channel to 7 before the television has processed either message.  The television may have both change request messages in it’s TCP kernel buffer.  From a UX standpoint it is not ideal that the television rapidly jump between two channels.  FoxMQ provides a way to implement a better UX using the publish count and id in the message header.  When the television processes the change request from client A, it will publish the new channel as 5.  The television will also increment the publish count and set the id in the publish message equal to the id of client A.  When the television processes the change request from client B, it can check if the publish count in the change request equals the current publish count.  If not, it can optionally discard the change request as it is identified as out of date.  Client B will be expecting the id in the newly published channel message to equal it’s id and it will not.  This is how client B can know that another client changed the channel before it and inform the user appropriately.

### Broadcast

The server can send a broadcast message that is sent to all connected clients regardless of their subscription.  A broadcast message has a publish action type and is received in the client in the same mechanism as a publish.

### Post-Response

Clients can post a topic with or without content to the server and then the server only responds to that one client with the response.  (like http post) This is the post action type.  The FoxMQ den library emits the incomingPost signal / callback when a cleint sends a post to the server.  The server must process the post and send back a post-response, ack or nack action type on the same topic to the client.

## User Access Control

Users are assigned a list of different topic prefixes and / or whole topics depending upon their permissions.  Clients are assigned the permitted topics as they connect or authenticate.  Permitted topics can be updated throughout the connection.  (for example, more topics can be permitted after authentication ).  Setting a topic = "user" also allows that client to access topics = "user.add", "user.remove", "user.ro.list" (user*).  Setting a topic = "user.ro" would only allow that client to access "user.ro.list" (user.ro*)

## Quick Start Guide

These code snipets are taken from the example.  See example dir for example client and server.  Open multiple clients for best experience.

#### Server API (Den)

Add the server api header.  Den, as in, a den of foxes :)
```c++
#include "foxmqden.h"
```
Create an instance of FoxmqDen in the project somewhere, for example in MyComms

```c++
class MyComms
{
private:
    FoxmqDen den;
};
```
Start the den

```c++
MyComms::open()
{
    connect(&den, &FoxmqDen::newClient, this, &MyComms::newClient);
    connect(&den, &FoxmqDen::clientDisconnected, this, &MyComms::clientDisconnected);
    connect(&den, &FoxmqDen::contentChange, this, &MyComms::contentChange);
    connect(&den, &FoxmqDen::incomingPost, this, &MyComms::incomingPost);
    den.startSSLSocketsWithoutEncryption(11401);
    den.publish("tv-channel.current", FoxHead::ContentType::IntAsString, "11");
};

void MyComms::newClient(FoxID clientID, bool authenticated)
{
    FoxPermittedPrefixes prefixes = {"tv-channel"};
    den.setPermittedTopics(clientID, prefixes);
};

void MyComms::contentChange(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content)
{
    bool current = publishCount == den.publishCount(topic);
    if (topic == "tv-channel.current") {
        if (current) {
            channelString = content;
            den.updateContentFromChangeRequest(topic, clientID, contentType, content);
        }
    } else {
        den.sendUnknownTopic(clientID, topic);
    }
};

void MyComms::incomingPost(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content)
{
    if (topic == "tv-channel.lineup") {
        QByteArray reply;
        if (content == "11") {
            reply = "The Simpsons - 7pm\nX-Files - 8pm";
        } else if (content == "5") {
            reply = "Revolution - 7pm\nMoney Heist - 8pm";
        } else if (content == "7") {
            reply = "Star Trek - 7pm\nThe Joy of Painting - 8pm";
        }
        den.answerPost(clientID, topic, FoxHead::ContentType::String, reply);
    } else {
        den.sendUnknownTopic(clientID, topic);
    }
}

```
#### Client API

Add the client api header.
```c++
#include "foxmq.h"
```
Create an instance of FoxMQ in the project somewhere, for example in Window

```c++
class Window : public QWidget
{
private:
    FoxMQ foxmq;
};
```
Connect to the Den

```c++
Window::Window()
{
    connect(&foxmq, &FoxMQ::ready, this, &Window::foxmqReady);
    connect(&foxmq, &FoxMQ::publish, this, &Window::incomingPublish);
    connect(&foxmq, &FoxMQ::postResponse, this, &Window::postResponse);
    connect(&foxmq, &FoxMQ::closed, this, &Window::serverClosed);
    foxmq.connectToDenWithoutEncryption("127.0.0.1", 11401);
}

void Window::foxmqReady()
{
    foxmq.subscription("tv-channel.current", true);
}

void Window::incomingPublish(const QByteArray &topic, int publishCount, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
    if (topic == "tv-channel.current") {
        currentChannel->setText(content);  // currentChannel is a QLabel
        foxmq.post("tv-channel.lineup", FoxHead::ContentType::String, content);  // ask the server for the channel lineup for this specific channel
    }
}

void Window::postResponse(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
    if (topic == "tv-channel.lineup")
        lineup->setText(content);  // lineup is a QLabel
}

void Window::serverClosed()
{
    currentChannel->setText("Server Closed");
}

```

## Building

FoxMQ can be built directly into your project if your project is [LGPL3](http://www.gnu.org/licenses/lgpl-3.0.en.html) compatible.  If your project is closed source, you can build FoxMQ as a dynamic library and link against it.

### Building into your project

1. Clone, submodule add or download FoxMQ.  If you download, unzip and copy the foxmq directory to be under your project's directory
2. For server applications include the foxmq/den/den.pri file in your projects .pro file

    include(foxmq/den/den.pri)

3. For client applications include the foxmq/client/client.pri file in your projects .pro file

    include(foxmq/client/client.pri)

4. Add QFOXMQ_STATIC define in your projects .pro file

    DEFINES+= QFOXMQ_STATIC

### Compiling as a dynamic library

1. Clone or download FoxMQ.  If you download, unzip.
2. Create a shadow build dir, enter the shadow build dir.
3. run qmake path_to_foxmq_download/qtfoxmq.pro and then make.

## Distribution Requirements

Distributing Software / Apps that use FoxMQ must follow the requirements of the LGPLv3.  Some of my interpretations of the LGPLv3


* LGPLv3 text or a link to the LGPLv3 text must be distributed with the binary.  My preferred way to do this is in the App's "About" section.
* An offer of source code with a link to FoxMQ must be distributed with the binary.  My preferred way to do this is in the App's "About" section
* For Android and iOS apps only, instructions on how to re-link or re-package the App or a link to instructions must be distributed with the binary.  My preferred way to do this is in the App's "About" section.


All of the above must be shown to the user at least once, separate from the EULA, with a method for the user to acknowledge they have seen it (an ok button).  Ideally all of the above is also listed in the description of the App on the App Store.

## Apple App Store deployment

Publishing closed source Apps that use a LGPLv3 library in the Apple App Store must provide a method for the end user to 1. update the library in the app and 2. run the new version of the app with the updated library.  Qt on iOS further complicates this by using static linking.  Closed source Apps on iOS using FoxMQ must provide the apps object files along with clear step by step instructions on how to re-link the app with a new / different version of FoxMQ (obligation 1).  iOS end uses can run the re-linked App on their device by creating a free iOS developer account and use the time limited signing on that account for their device.  (obligation 2)  I consider this an poor way to meet obligation 2, but as long as Apple has this mechanism, obligation 2 is meet.  I will not pursue copyright infringement as long as the individual / organization is meeting obligation 1 and 2 and the Distribution Requirements above.
