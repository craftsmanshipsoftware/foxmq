QT = core network

include($$PWD/den/den.pri)
include($$PWD/client/client.pri)

#VERSION = 1.0

TEMPLATE = lib
TARGET = $$qtLibraryTarget(QtFoxMQ$$QT_LIBINFIX)
CONFIG += module create_prl
DEFINES+= QT_BUILD_FOXMQ_LIB
mac:QMAKE_FRAMEWORK_BUNDLE_NAME = $$TARGET
INCLUDEPATH+= $$PWD/common

headersDataFiles.files+= $$PWD/common/foxhead.h
headersDataFiles.files+= $$PWD/common/foxid.h
headersDataFiles.files+= $$PWD/den/foxmqden.h
headersDataFiles.files+= $$PWD/client/foxmq.h

# install to Qt installation directory if no PREFIX specified
_PREFIX = $$PREFIX
isEmpty(_PREFIX) {
	INSTALL_HEADER_PATH = $$[QT_INSTALL_HEADERS]/QtFoxMQ/
	INSTALL_LIB_PATH = $$[QT_INSTALL_LIBS]
} else {

	INSTALL_HEADER_PATH = $$PREFIX/include/QtFoxMQ/
	INSTALL_LIB_PATH = $$PREFIX/lib
}
message(install to: $$INSTALL_LIB_PATH)
headersDataFiles.path = $$INSTALL_HEADER_PATH
target.path = $$INSTALL_LIB_PATH

INSTALLS+= target headersDataFiles
