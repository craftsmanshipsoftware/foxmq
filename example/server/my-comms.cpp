//#include <QDebug>
#include "my-comms.h"

MyComms::MyComms()
{
	connect(&den, &FoxmqDen::newClient, this, &MyComms::newClient);
	connect(&den, &FoxmqDen::clientDisconnected, this, &MyComms::clientDisconnected);
	connect(&den, &FoxmqDen::contentChange, this, &MyComms::contentChange);
	connect(&den, &FoxmqDen::incomingPost, this, &MyComms::incomingPost);
	den.startSSLSocketsWithoutEncryption(11401);
	den.publish("tv-channel.current", FoxHead::ContentType::IntAsString, "11");
	den.publish("tv-channel.last", FoxHead::ContentType::IntAsString, "19");
}

void MyComms::newClient(FoxID clientID, bool authenticated)
{
	Q_UNUSED(authenticated);

	qDebug() << "Server - Client" << clientID.local() << "Connected";
	FoxPermittedPrefixes prefixes = {"tv-channel"};  // allow all topics starting with tv-channel
	den.setPermittedTopics(clientID, prefixes);
};

void MyComms::clientDisconnected(FoxID clientID)
{
	qDebug() << "Server - Client" << clientID.local() << "Disconnected";
}

void MyComms::contentChange(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content)
{
	bool current = publishCount == den.publishCount(topic);
	if (topic == "tv-channel.current") {
		if (current) {
			lastChannelString = channelString;
			channelString = content;
			den.updateContentFromChangeRequest(topic, clientID, contentType, content);
			den.publish("tv-channel.last", FoxHead::ContentType::IntAsString, lastChannelString);
		}
	} else {
		den.sendUnknownTopic(clientID, topic);
	}
};

void MyComms::incomingPost(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content)
{
	Q_UNUSED(publishCount);
	Q_UNUSED(contentType);

	if (topic == "tv-channel.lineup") {
		QByteArray reply;
		if (content == "11") {
			reply = "The Simpons - 7pm\nX-Files - 8pm";
		} else if (content == "5") {
			reply = "Revolution - 7pm\nMoney Heist - 8pm";
		} else if (content == "7") {
			reply = "Star Trek - 7pm\nThe Joy of Painting - 8pm";
		}
		den.answerPost(clientID, topic, FoxHead::ContentType::String, reply);
	} else {
		den.sendUnknownTopic(clientID, topic);
	}
}
