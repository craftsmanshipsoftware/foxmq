#include <QDebug>
#include <QCoreApplication>
#include "my-comms.h"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	QStringList args = app.arguments();

	MyComms myComms;

	return app.exec();
}
