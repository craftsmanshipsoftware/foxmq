#ifndef MY_COMMS
#define MY_COMMS
#include <QObject>
#include <QTimer>
#include "foxmqden.h"

class MyComms : public QObject
{
	Q_OBJECT

public:
	MyComms();

private:
	void newClient(FoxID clientID, bool authenticated);
	void clientDisconnected(FoxID clientID);
	void contentChange(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content);
	void incomingPost(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content);
	FoxmqDen den;
	QByteArray channelString;
	QByteArray lastChannelString;
};

#endif
