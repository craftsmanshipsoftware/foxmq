#include <QWidget>
#include <QLabel>
#include "foxmq.h"

class Window : public QWidget
{
	Q_OBJECT
public:
	Window();

private:
	void b1Clicked();
	void b2Clicked();
	void b3Clicked();
	void foxmqReady();
	void incomingPublish(const QByteArray &topic, int publishCount, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void postResponse(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void serverClosed();
	QLabel *currentChannel;
	QLabel *lastChannel;
	QLabel *lineup;
	FoxMQ foxmq;
};
