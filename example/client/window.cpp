#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include "window.h"

Window::Window()
{
	// build GUI
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QGridLayout *channelLayout = new QGridLayout;
	layout->addLayout(channelLayout);
	QLabel *title = new QLabel("Current CH:");
	channelLayout->addWidget(title, 0, 0);
	channelLayout->setAlignment(title, Qt::AlignRight);
	title = new QLabel("Last CH:");
	channelLayout->addWidget(title, 1, 0);
	channelLayout->setAlignment(title, Qt::AlignRight);
	currentChannel = new QLabel("Unconnected");
	channelLayout->addWidget(currentChannel, 0, 1);
	channelLayout->setAlignment(currentChannel, Qt::AlignLeft);
	lastChannel = new QLabel("Unconnected");
	channelLayout->addWidget(lastChannel, 1, 1);
	channelLayout->setAlignment(lastChannel, Qt::AlignLeft);

	layout->addStretch(1);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	layout->addLayout(buttonLayout);

	buttonLayout->addStretch(1);
	QPushButton *b1 = new QPushButton("Ch 11");
	buttonLayout->addWidget(b1);
	buttonLayout->addStretch(1);
	QPushButton *b2 = new QPushButton("Ch 7");
	buttonLayout->addWidget(b2);
	buttonLayout->addStretch(1);
	QPushButton *b3 = new QPushButton("Ch 5");
	buttonLayout->addWidget(b3);
	buttonLayout->addStretch(1);

	layout->addStretch(1);
	lineup = new QLabel("");
	layout->addWidget(lineup);
	layout->setAlignment(lineup, Qt::AlignHCenter);

	layout->addStretch(1);

	connect(b1, &QPushButton::clicked, this, &Window::b1Clicked);
	connect(b2, &QPushButton::clicked, this, &Window::b2Clicked);
	connect(b3, &QPushButton::clicked, this, &Window::b3Clicked);

	//FoxMQ setup
	connect(&foxmq, &FoxMQ::ready, this, &Window::foxmqReady);
	connect(&foxmq, &FoxMQ::publish, this, &Window::incomingPublish);
	connect(&foxmq, &FoxMQ::postResponse, this, &Window::postResponse);
	connect(&foxmq, &FoxMQ::closed, this, &Window::serverClosed);
	foxmq.connectToDenWithoutEncryption("127.0.0.1", 11401);

	show();
}

void Window::b1Clicked()
{
	foxmq.contentChange("tv-channel.current", FoxHead::IntAsString, "11");
}

void Window::b2Clicked()
{
	foxmq.contentChange("tv-channel.current", FoxHead::IntAsString, "7");
}

void Window::b3Clicked()
{
	foxmq.contentChange("tv-channel.current", FoxHead::IntAsString, "5");
}

void Window::foxmqReady()
{
	qDebug() << "Conected to Den.  My ID is" << foxmq.id();
	foxmq.subscription("tv-channel.current", true);
	foxmq.subscription("tv-channel.last", true);
}

void Window::incomingPublish(const QByteArray &topic, int publishCount, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
	Q_UNUSED(contentType);

	qDebug() << "Got Publish" << topic << "Publish Count" << publishCount << "set By client" << id.local();
	if (topic == "tv-channel.current") {
		currentChannel->setText(content);
		foxmq.post("tv-channel.lineup", FoxHead::ContentType::String, content);
	} else if (topic == "tv-channel.last") {
		lastChannel->setText(content);
	}
}

void Window::postResponse(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
	Q_UNUSED(id);
	Q_UNUSED(contentType);

	qDebug() << topic << content;
	if (topic == "tv-channel.lineup")
		lineup->setText(content);
}

void Window::serverClosed()
{
	currentChannel->setText("Server Closed");
	lastChannel->clear();
	lineup->clear();
}
