#include <QDebug>
#include <QApplication>
#include "window.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QStringList args = app.arguments();

	Window window;

	return app.exec();
}
