/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar, Jonathan Bagg

*************************************************************************/
#include "foxmq.h"
#include "../common/foxhead.h"

#include <QByteArray>

bool FoxMQ::contentChange(const QByteArray &topic, FoxHead::ContentType content_type, const QByteArray &content)
{
	if (!isConnected()) {
		return false;
	}

	// Get the publish count for the given topic
	int publish_count = getPublishCount(topic);

	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::ChangeRequest);
	header.setPublishCount(publish_count);
	header.setContentLength(content.size());
	header.setContentType(content_type);
	header.setId(client_id_);
	header.setTopicLength(topic.size());

	QByteArray msg = header_data + topic + content;

	sslSocket->write(msg);

	return true;
}

bool FoxMQ::getContent(const QByteArray &topic)
{
	if (!isConnected()) {
		return false;
	}

	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::Ask);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	header.setId(client_id_);
	header.setTopicLength(topic.size());

	sslSocket->write(header_data + topic);

	return true;
}

bool FoxMQ::post(const QByteArray &topic, FoxHead::ContentType content_type, const QByteArray &content)
{
	if (!isConnected()) {
		return false;
	}

	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::Post);
	header.setPublishCount(0);
	header.setContentLength(content.size());
	header.setContentType(content_type);
	header.setId(client_id_);
	header.setTopicLength(topic.size());

	sslSocket->write(header_data + topic + content);

	return true;
}

bool FoxMQ::subscription(const QByteArray &topic, bool enable)
{
	if (!isConnected()) {
		return false;
	}

	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(enable ? FoxHead::Subscribe : FoxHead::Unsubscribe);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	header.setId(client_id_);
	header.setTopicLength(topic.size());

	sslSocket->write(header_data + topic);

	return true;
}

void FoxMQ::connectToDen(const QHostAddress &ip, quint16 port, const QSslConfiguration &config)
{
	connectToDen(ip.toString().toUtf8(), port, config);
}

void FoxMQ::connectToDen(const QByteArray &ip, quint16 port, const QSslConfiguration &config)
{
	sslSocket = QSharedPointer<QSslSocket>::create();
	sslSocket->setSslConfiguration(config);
	sslSocket->connectToHostEncrypted(ip, port);
	QObject::connect(sslSocket.data(), &QSslSocket::connected, this, &FoxMQ::onConnected);
	QObject::connect(sslSocket.data(), &QSslSocket::disconnected, this, &FoxMQ::denDisconnected);
	QObject::connect(sslSocket.data(), &QSslSocket::readyRead, this, &FoxMQ::readReady);
	connect(sslSocket.data(), &QSslSocket::errorOccurred, [](QAbstractSocket::SocketError error) {
		qInfo() << "FoxMQ SSl connect error" << error;
	});
}


void FoxMQ::connectToDenWithoutEncryption(const QHostAddress &ip, quint16 port)
{
	connectToDenWithoutEncryption(ip.toString().toUtf8(), port);
}

void FoxMQ::connectToDenWithoutEncryption(const QByteArray &ip, quint16 port)
{
	// connect to the ip
	sslSocket.reset(new QSslSocket());
	sslSocket->connectToHost(ip, port);
	QObject::connect(sslSocket.data(), &QSslSocket::connected, this, &FoxMQ::onConnected);
	QObject::connect(sslSocket.data(), &QSslSocket::disconnected, this, &FoxMQ::denDisconnected);
	QObject::connect(sslSocket.data(), &QSslSocket::readyRead, this, &FoxMQ::readReady);
}

void FoxMQ::onConnected()
{
	m_isConnected = true;
}

void FoxMQ::disconnectFromDen()
{
	if (!m_isConnected) {
		qDebug("FoxMQ Client - den already disconnected");
		return;
	}
	sslSocket->disconnect();
	sslSocket->close();
	m_isConnected = false;
}

void FoxMQ::denDisconnected()
{
	m_isConnected = false;
	emit closed();
}

void FoxMQ::readReady()
{
	while (sslSocket->bytesAvailable() >= nextLength) {
		switch (state) {
			case State::HeadNext: {
				QByteArray header_data = sslSocket->read(nextLength);
				FoxHead header(header_data.data());
				if (!header.checkTag()) {
					disconnectFromDen();
					return;
				}
				action = header.action();
				topicLength = header.topicLength();
				contentLength = header.contentLength();
				count = header.publishCount();
				incomingID = header.id();
				contentType = header.contentType();
				nextLength = topicLength;
				if (action == FoxHead::Action::Id) {
					client_id_ = incomingID;
					emit ready();
				}
				state = State::TopicNext;
				break;
			}
			case State::TopicNext: {
				topic = sslSocket->read(nextLength);
				if (action == FoxHead::Action::PostResponse || action == FoxHead::Action::Publish) {
					nextLength = contentLength;
					state = State::ContentNext;
				} else {
					if (action == FoxHead::Action::Ack) {
						emit ack(topic);
					} else if (action == FoxHead::Action::Nack) {
						emit nack(topic);
					} else if (action == FoxHead::Action::ErrorNoPermission) {
						emit errorNoPermission(topic);
					} else if (action == FoxHead::Action::ErrorUnknownTopic) {
						emit errorUnknownTopic(topic);
					} else if (action == FoxHead::Action::Error) {
						emit error(topic);
					}
					nextLength = FoxHead::size();
					state = State::HeadNext;
				}
				break;
			}
			case State::ContentNext: {
				content = sslSocket->read(nextLength);
				if (action == FoxHead::Action::Publish) {
					m_publishCounts[topic] = count;
					emit publish(topic, count, incomingID, contentType, content);
				} else if (action == FoxHead::Action::PostResponse) {
					emit postResponse(topic, incomingID, contentType, content);
				}
				nextLength = FoxHead::size();
				state = State::HeadNext;
				break;
			}
		}
	}
}
