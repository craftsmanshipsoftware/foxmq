/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOXMQ_H
#define FOXMQ_H

#include <QByteArray>
#include <QtCore/QObject>
#include <QSslSocket>
#include <QSslConfiguration>
#include <QHostAddress>

#include "../common/foxhead.h"

#if (!defined(QT_STATIC) && !defined(QFOXMQ_STATIC))
#       ifdef QT_BUILD_FOXMQ_LIB
#               define Q_FOXMQ_EXPORT Q_DECL_EXPORT
#       else
#       define Q_FOXMQ_EXPORT Q_DECL_IMPORT
#       endif
#else
#       define Q_FOXMQ_EXPORT
#endif

inline QDebug operator<<(QDebug dbg, FoxID id)
{
	dbg.nospace() << QString("%1:%2:%3:%4:%5:%6:%7")
						 .arg(id.mac[0], 2, 16, QChar('0'))
						 .arg(id.mac[1], 2, 16, QChar('0'))
						 .arg(id.mac[2], 2, 16, QChar('0'))
						 .arg(id.mac[3], 2, 16, QChar('0'))
						 .arg(id.mac[4], 2, 16, QChar('0'))
						 .arg(id.mac[5], 2, 16, QChar('0'))
						 .arg(id.client_id, 16, 16, QChar('0'));
	return dbg.space();
}

class Q_FOXMQ_EXPORT FoxMQ : public QObject
{
	Q_OBJECT
public:
	void connectToDen(const QByteArray &ip, quint16 port, const QSslConfiguration &config);
	void connectToDen(const QHostAddress &ip, quint16 port, const QSslConfiguration &config);
	void connectToDenWithoutEncryption(const QByteArray &ip, quint16 port);
	void connectToDenWithoutEncryption(const QHostAddress &ip, quint16 port);
	bool contentChange(const QByteArray &topic, FoxHead::ContentType content_type, const QByteArray &content);
	bool getContent(const QByteArray &topic);
	bool post(const QByteArray &topic, FoxHead::ContentType content_type, const QByteArray &content);
	bool subscription(const QByteArray &topic, bool enable);
	void disconnectFromDen();
	FoxID id() { return client_id_; }
	bool isConnected() { return m_isConnected; }
	int getPublishCount(const QByteArray &topic) { return m_publishCounts[topic]; }

private:
	enum class State {
		HeadNext,
		TopicNext,
		ContentNext,
	};
	QSharedPointer<QSslSocket> sslSocket;
	State state = State::HeadNext;
	FoxHead::Action action;
	QByteArray topic;
	QByteArray content;
	ssize_t count;
	FoxID incomingID;
	FoxHead::ContentType contentType;
	ssize_t nextLength = FoxHead::size();
	ssize_t topicLength;
	ssize_t contentLength;
	FoxID client_id_;
	bool m_isConnected = false;
	QMap<QByteArray, int> m_publishCounts;
Q_SIGNALS:
	void closed();
	void ready();
	void publish(const QByteArray &topic, int publishCount, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void postResponse(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void errorUnknownTopic(QByteArray topic);
	void errorNoPermission(QByteArray topic);
	void error(QByteArray topic);
	void ack(QByteArray topic);
	void nack(QByteArray topic);

private Q_SLOTS:
	void onConnected();
	void denDisconnected();
	void readReady();
};

#endif  // FOXMQ_H
