/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOXHEAD_H
#define FOXHEAD_H

#include <QtGlobal>
#include <QtEndian>
#include <optional>
#include "foxid.h"

struct FoxHead {
public:
	enum Action { Id, Ack, Nack, ErrorNoPermission, ErrorUnknownTopic, Error, Subscribe, Unsubscribe, Publish, ChangeRequest, Ask, Post, PostResponse, CustomStart = 32 };
	enum ContentType {
		Empty,
		Bool,
		Int,
		IntAsString,
		Float,
		FloatAsString,
		String,
		Blob,
		Json,
		Xml,
		Jpg,
		Gif,
		Png,
	};
	/*
	‘F’ ‘M’ ‘Q’ tag,     3 bytes
	Action,              1 byte
	asker ID             14 bytes
	topic publish count  4 bytes
	content length       4 bytes
	content type         1 byte
	topic length         1 byte
	*/
	void setTag()
	{
		_data[0] = 'F';
		_data[1] = 'M';
		_data[2] = 'Q';
	}
	bool checkTag() { return _data[0] == 'F' && _data[1] == 'M' && _data[2] == 'Q'; }

	static const int ACTION_OFFSET = 3;

	Action action() { return (Action)_data[ACTION_OFFSET]; }
	void setAction(Action action) { _data[ACTION_OFFSET] = action; }

	static const int ID_OFFSET = ACTION_OFFSET + 1;
	FoxID id()
	{
		FoxID ret;
		memcpy(ret.mac, &_data[ID_OFFSET], 6);
		ret.client_id = qFromLittleEndian<uint64_t>(&_data[ID_OFFSET + 6]);
		return ret;
	}
	void setId(FoxID id)
	{
		memcpy(&_data[ID_OFFSET], id.mac, 6);
		qToLittleEndian<uint64_t>(id.local(), &_data[ID_OFFSET + 6]);
	}

	// gcc bug. bogus warnings when building release    https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94335
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wstringop-overflow"

	static const int PUBLISH_COUNT_OFFSET = ID_OFFSET + 14;
	quint32 publishCount() { return qFromLittleEndian<quint32>(&_data[PUBLISH_COUNT_OFFSET]); }
	void setPublishCount(quint32 count) { qToLittleEndian(count, &_data[PUBLISH_COUNT_OFFSET]); }

	static const int CONTENT_LENGTH_OFFSET = PUBLISH_COUNT_OFFSET + 4;
	quint32 contentLength() { return qFromLittleEndian<quint32>(&_data[CONTENT_LENGTH_OFFSET]); }
	void setContentLength(quint32 length) { qToLittleEndian(length, &_data[CONTENT_LENGTH_OFFSET]); }

	static const int CONTENT_TYPE_OFFSET = CONTENT_LENGTH_OFFSET + 4;
	ContentType contentType() { return (ContentType)_data[CONTENT_TYPE_OFFSET]; }  // XXX: is casting ok here?
	void setContentType(ContentType contentType) { _data[CONTENT_TYPE_OFFSET] = contentType; }

	static const int TOPIC_LENGTH_OFFSET = CONTENT_TYPE_OFFSET + 1;
	char topicLength() { return _data[TOPIC_LENGTH_OFFSET]; }
	void setTopicLength(char topicLength) { _data[TOPIC_LENGTH_OFFSET] = topicLength; }

	#pragma GCC diagnostic pop

	static size_t size() { return TOPIC_LENGTH_OFFSET + 1; }

private:
	char *_data;

public:
	FoxHead(char *data) : _data(data) {}
	static std::optional<FoxHead> create(QByteArray &msg)
	{
		if (msg.length() < 20) {
			return std::nullopt;
		}
		FoxHead m(msg.data());
		return std::optional(m);
	}
};

#endif  // FOXHEAD_H
