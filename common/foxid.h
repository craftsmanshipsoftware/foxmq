/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOXID_H
#define FOXID_H
#include <stdint.h>

class FoxID
{
public:
	FoxID() {}
	uint64_t local() const { return client_id; }

	FoxID &operator++(int)
	{
		client_id++;
		return *this;
	}

	uint8_t mac[6] = {};
	uint64_t client_id = 0;

	bool operator==(const FoxID &other)
	{
		return client_id == other.client_id && mac[0] == other.mac[0] && mac[1] == other.mac[1] && mac[2] == other.mac[2] && mac[3] == other.mac[3] && mac[4] == other.mac[4]
		       && mac[5] == other.mac[5];
	}

	bool operator!=(const FoxID &other)
	{
		return client_id != other.client_id || mac[0] != other.mac[0] || mac[1] != other.mac[1] || mac[2] != other.mac[2] || mac[3] != other.mac[3] || mac[4] != other.mac[4]
		       || mac[5] != other.mac[5];
	}
};

#endif  // FOXID_H
