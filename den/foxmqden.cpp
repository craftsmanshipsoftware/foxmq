/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar, Jonathan Bagg

*************************************************************************/

#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "foxmqden.h"
#include "QtWebSockets/qwebsocketserver.h"
#include "QtWebSockets/qwebsocket.h"
#include <QtCore/QDebug>
#include <QNetworkInterface>
#include <QSslSocket>
#include "foxwebsocket.h"
#include "foxsslsocket.h"

QT_USE_NAMESPACE

FoxmqDen::FoxmqDen()
{
	// find the first network interface with a MAC address and use the MAC address for the Den's ID.
	for (auto iface : QNetworkInterface::allInterfaces()) {
		if (iface.type() == QNetworkInterface::Ethernet || iface.type() == QNetworkInterface::Wifi) {
			QByteArray mac = QByteArray::fromHex(iface.hardwareAddress().toLatin1());
			for (int i=0; i<6; i++)
				serverID.mac[i] = mac.at(i);
			nextClientID = serverID;
			nextClientID++;
			break;
		}
	}
}

bool FoxmqDen::startSSLSockets(quint16 port, const QSslConfiguration &sslConfig)
{
	if (isListening()) {
		qInfo() << "FoxMQ already listening on port" << port;
		return false;
	} else {
		sslEncryptionEnabled = true;
		sslConfiguration = sslConfig;
		return listen(QHostAddress::Any, port);
	}
}

bool FoxmqDen::startSSLSocketsWithoutEncryption(quint16 port)
{
	if (isListening()) {
		qInfo() << "FoxMQ already listening on port" << port;
		return false;
	} else {
		sslEncryptionEnabled = false;
		return listen(QHostAddress::Any, port);
	}
}

void FoxmqDen::stopSSLSockets()
{
	close();
}

bool FoxmqDen::startWebSockets(const QString &name, quint16 port, const QSslConfiguration &sslConfig)
{
	bool success = false;
	if (m_pWebSocketServer.isNull()) {
		m_pWebSocketServer = QSharedPointer<QWebSocketServer>::create(name, QWebSocketServer::SslMode::NonSecureMode);
		connect(m_pWebSocketServer.data(), &QWebSocketServer::newConnection, this, &FoxmqDen::onNewWebSocketConnection);
		m_pWebSocketServer->setSslConfiguration(sslConfig);
		success = m_pWebSocketServer->listen(QHostAddress::Any, port);
	} else {
		qInfo() << "FoxMQ failed to listen on port" << port;
		return false;
	}
	return success;
}

bool FoxmqDen::startWebSocketsWithoutEncryption(const QString &name, quint16 port)
{
	bool success = false;
	if (m_pWebSocketServer.isNull()) {
		m_pWebSocketServer = QSharedPointer<QWebSocketServer>::create(name, QWebSocketServer::SslMode::NonSecureMode);
		connect(m_pWebSocketServer.data(), &QWebSocketServer::newConnection, this, &FoxmqDen::onNewWebSocketConnection);
		success = m_pWebSocketServer->listen(QHostAddress::Any, port);
	} else {
		qInfo() << "FoxMQ failed to listen on port" << port;
		return false;
	}
	return success;
}

void FoxmqDen::stopWebSockets()
{
	m_pWebSocketServer->close();
	m_pWebSocketServer.clear();
}

void FoxmqDen::setPermittedTopics(FoxID client, std::unordered_set<QByteArray> permittedTopics)
{
	if (clients.contains(client.local())) {
		clients[client.local()]->setPermittedTopics(permittedTopics);
	}
}

void FoxmqDen::incomingConnection(qintptr socketDescriptor)
{
	QSslSocket *socket = new QSslSocket;
	socket->setSocketDescriptor(socketDescriptor);
	socket->setSocketOption(QSslSocket::KeepAliveOption, 1);
#ifdef Q_OS_DARWIN
	int enabled = 1;
	int seconds = 7200;
	setsockopt(socketDescriptor, SOL_SOCKET,  SO_KEEPALIVE, &enabled, sizeof enabled);
	setsockopt(socketDescriptor, IPPROTO_TCP, TCP_KEEPALIVE, &seconds, sizeof seconds);
#else
	int cnt = 9;
	int idle = 7200;
	int interval = 75;
	setsockopt(socketDescriptor, IPPROTO_TCP, TCP_KEEPCNT, &cnt, sizeof(int));
	setsockopt(socketDescriptor, IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(int));
	setsockopt(socketDescriptor, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(int));
#endif

	connect(socket, &QSslSocket::encrypted, this, &FoxmqDen::sslSocketReady);
	connect(socket, QOverload<const QList<QSslError> &>::of(&QSslSocket::sslErrors), [=](const QList<QSslError> &errors) {
		socket->ignoreSslErrors(errors);  // allow non athenticated connections, but flag them in sslSocketReady().  All connections are encrypted.
	});
	if (sslEncryptionEnabled) {
		socket->setSslConfiguration(sslConfiguration);
		socket->startServerEncryption();
	} else {
		setupConnection(socket);
	}
}

void FoxmqDen::sslSocketReady()
{
	setupConnection(qobject_cast<QSslSocket*>(sender()));
}

void FoxmqDen::setupConnection(QSslSocket *socket)
{
	auto client = QSharedPointer<FoxSslSocket>::create(socket, *this, nextClientID++);
	if (!socket->sslHandshakeErrors().size())
		client->authenticated = true;
	clients[client->id().local()] = client;
	emit newClient(client->id(), false);
	client->sendID();
}

void FoxmqDen::onNewWebSocketConnection()
{
	QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

	auto socket = QSharedPointer<FoxWebSocket>::create(pSocket, *this, nextClientID++);
	clients[socket->id().local()] = socket;
	emit newClient(socket->id(), false);
	socket->sendID();
}

void FoxmqDen::processMessage(FoxHead::Action action, uint32_t publishCount, FoxID clientID, QByteArray topic, FoxHead::ContentType contentType, QByteArray content,
                              QSharedPointer<Fox> client)
{
	if (!client->hasPermission(topic)) {
		sendNoContent(clientID, FoxHead::Action::ErrorNoPermission, topic);
		return;
	}
	switch (action) {
		case FoxHead::Action::ChangeRequest: {
			emit contentChange(topic, publishCount, clientID, contentType, content);
			break;
		}
		case FoxHead::Action::Post: {
			emit incomingPost(topic, publishCount, clientID, contentType, content);
			break;
		}
		default: break;
	}
}

void FoxmqDen::sendNoContent(FoxID clientID, FoxHead::Action action, QByteArray topic)
{

	if (!clients.contains(clientID.local())) {
		return;
	}
	auto client = clients[clientID.local()];
	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(action);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	header.setId(clientID);
	header.setTopicLength(topic.size());
	client->sendData(header_data + topic);
}

void FoxmqDen::sendUnknownTopic(FoxID client_id, QByteArray topic)
{
	sendNoContent(client_id, FoxHead::Action::ErrorUnknownTopic, topic);
}

void FoxmqDen::sendError(FoxID client_id, QByteArray topic)
{
	sendNoContent(client_id, FoxHead::Action::Error, topic);
}

void FoxmqDen::ack(FoxID client_id, QByteArray topic)
{
	sendNoContent(client_id, FoxHead::Action::Ack, topic);
}

void FoxmqDen::nack(FoxID client_id, QByteArray topic)
{
	sendNoContent(client_id, FoxHead::Action::Nack, topic);
}

bool FoxmqDen::sendCurrentContent(const QByteArray &topic, const Fox &client)
{
	if (messages.contains(topic)) {
		client.sendMessage(*messages[topic]);
		return true;
	}
	return false;
}

QSharedPointer<FoxScream> FoxmqDen::lookupMessage(QByteArray topic)
{
	if (messages.contains(topic)) {
		return messages[topic];
	} else {
		auto msg = QSharedPointer<FoxScream>::create(FoxScream(topic));
		messages[topic] = msg;
		buildSubscriptionAssociationsForNewMessage(topic, msg);
		return msg;
	}
}

void FoxmqDen::buildSubscriptionAssociationsForNewMessage(const QByteArray &topic, QSharedPointer<FoxScream> msg)
{
	for (const auto &client : qAsConst(clients)) {
		for (const auto &subscribed_topic : client->subscriptions()) {
			if (isPartialTopic(topic, subscribed_topic)) {
				msg->addFox(client);
			}
		}
	}
}

void FoxmqDen::addSubscriptionAssociations(const QSharedPointer<Fox> &fox, QByteArray topic)
{
	for (const auto &msg : messages) {
		if (isPartialTopic(msg->topic(), topic)) {
			msg->addFox(fox);
			fox->sendMessage(*msg);
		}
	}
}
void FoxmqDen::removeSubscriptionAssociations(const QSharedPointer<Fox> &fox, QByteArray topic)
{
	for (const auto &msg : messages) {
		if (isPartialTopic(msg->topic(), topic)) {
			msg->eraseFox(fox);
		}
	}
}

void FoxmqDen::updateContentFromChangeRequest(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
	sendMessageToSubscribedClients(topic, id, contentType, content);
}

void FoxmqDen::removeMessage(const QByteArray &topic)
{
	if (messages.contains(topic)) {
		messages.remove(topic);
	}
}

uint32_t FoxmqDen::publishCount(const QByteArray &topic)
{
	if (messages.contains(topic)) {
		return messages[topic]->publishCount();
	} else {
		return 0;
	}
}

void FoxmqDen::sendMessageToSubscribedClients(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
	lookupMessage(topic)->publish(id, contentType, content);
}

void FoxmqDen::disconnectClient(uint64_t local)
{
	QSharedPointer<Fox> client = clients.value(local);
	disconnectClient(client);
}

void FoxmqDen::disconnectClient(QSharedPointer<Fox> client)
{
	emit clientDisconnected(client->id());
	for (auto foxScream : messages) {
		foxScream->eraseFox(client);
	}
	client->prepareForDisconnect();
	// delete client after returning to the main loop as disconnectClient() is called from the client object and it needs to exist to return to it
	QMetaObject::invokeMethod(this, "deleteClient", Qt::QueuedConnection, Q_ARG(QSharedPointer<Fox>, client));
}

void FoxmqDen::deleteClient(QSharedPointer<Fox> client)
{
	clients.remove(client->id().local());
}

void FoxmqDen::broadcast(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content)
{

	QSharedPointer<FoxScream> message = lookupMessage(topic);
	message->setContent(serverID, contentType, content);

	for (auto &client : clients) {
		client->sendMessage(*message);
	}
}

void FoxmqDen::publish(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content)
{
	sendMessageToSubscribedClients(topic, serverID, contentType, content);
}

void FoxmqDen::publishBriefly(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content)
{
	sendMessageToSubscribedClients(topic, serverID, contentType, content);
	removeMessage(topic);
}

void FoxmqDen::answerPost(FoxID &clientID, const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content)
{
	if (!clients.contains(clientID.local())) {
		return;
	}
	auto client = clients[clientID.local()];
	// Construct the message header
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::Action::PostResponse);
	header.setPublishCount(0);
	header.setContentLength(content.length());
	header.setContentType(contentType);
	header.setId(serverID);
	header.setTopicLength(topic.size());
	client->sendData(header_data + topic + content);
}

QHostAddress FoxmqDen::getClientsIPAddress(const FoxID &clientID)
{
	QHostAddress ip;
	if (clients.contains(clientID.local()))
		ip = clients.value(clientID.local())->peerAddress();
	return ip;
}
