/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#include "foxsslsocket.h"
#include "foxmqden.h"
#include "../common/foxhead.h"

FoxSslSocket::FoxSslSocket(QSslSocket *socket, FoxmqDen &foxDen, FoxID id) : Fox(foxDen, id), sslSocket(socket)
{
	connect(sslSocket.data(), &QSslSocket::readyRead, this, &FoxSslSocket::readyRead);
	connect(sslSocket.data(), &QSslSocket::disconnected, this, &FoxSslSocket::disconnected);
}

void FoxSslSocket::readyRead()
{
	while (sslSocket->bytesAvailable() >= nextLength) {
		switch (state) {
			case State::HeadNext: {
				QByteArray header_data = sslSocket->read(nextLength);
				FoxHead header(header_data.data());
				if (!header.checkTag()) {
					sslSocket->close();
					return;
				}
				action = header.action();
				topicLength = header.topicLength();
				contentLength = header.contentLength();
				count = header.publishCount();
				incomingID = header.id();
				contentType = header.contentType();
				nextLength = topicLength;
				state = State::TopicNext;
				break;
			}
			case State::TopicNext: {
				topic = sslSocket->read(nextLength);
				if (action == FoxHead::Action::Subscribe) {
					if (hasPermission(topic))
						subscribe(topic);
					nextLength = FoxHead::size();
					state = State::HeadNext;
				} else if (action == FoxHead::Action::Unsubscribe) {
					unsubscribe(topic);
					nextLength = FoxHead::size();
					state = State::HeadNext;
				} else if (action == FoxHead::Action::Ask) {
					ask(topic);
					nextLength = FoxHead::size();
					state = State::HeadNext;
				} else {
					nextLength = contentLength;
					state = State::ContentNext;
				}
				break;
			}
			case State::ContentNext: {
				content = sslSocket->read(nextLength);
				_foxDen.processMessage(action, count, id(), topic, contentType, content, sharedFromThis());
				nextLength = FoxHead::size();
				state = State::HeadNext;
			}
		}
	}
}

void FoxSslSocket::sendMessage(FoxScream const &message) const
{
	sslSocket->write(message.asBinaryMessage());
}

void FoxSslSocket::sendData(const QByteArray data)
{
	sslSocket->write(data);
}

void FoxSslSocket::sendID()
{
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::Id);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	header.setId(id());
	header.setTopicLength(0);
	sendData(header_data);
}

void FoxSslSocket::disconnected()
{
	_foxDen.disconnectClient(sharedFromThis());
}
