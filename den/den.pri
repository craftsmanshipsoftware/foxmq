QT += websockets

include(../common/common.pri)

INCLUDEPATH+= $$PWD
SOURCES += \
    $$PWD/fox.cpp \
    $$PWD/foxmqden.cpp \
    $$PWD/foxscream.cpp \
    $$PWD/foxwebsocket.cpp \
    $$PWD/foxsslsocket.cpp \

HEADERS += \
    $$PWD/fox.h \
    $$PWD/foxmqden.h \
    $$PWD/foxscream.h \
    $$PWD/foxwebsocket.h \
    $$PWD/foxsslsocket.h \

