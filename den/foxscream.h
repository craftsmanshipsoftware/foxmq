/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOXSCREAM_H
#define FOXSCREAM_H
#include <QByteArray>
#include <QMap>
#include <QSharedPointer>
#include "../common/foxid.h"
#include "../common/foxhead.h"

class Fox;

enum FoxAssociation { AddOne, RemoveOne };

class FoxScream
{
public:
	explicit FoxScream(const QByteArray &topic);
	inline const QByteArray &head() { return _head; }
	inline const QByteArray &topic() { return _topic; }
	inline const QByteArray &content() { return _content; }
	inline ssize_t publishCount() { return _publishCount; }
	void addFox(const QSharedPointer<Fox> &client);
	void eraseFox(const QSharedPointer<Fox> &client);
	void publish(const FoxID &id, FoxHead::ContentType contentType, const QByteArray &content);
	void setContent(const FoxID &id, FoxHead::ContentType contentType, const QByteArray &content);
	QByteArray asBinaryMessage() const;

private:
	QByteArray _head;
	QByteArray _topic;
	QByteArray _content;
	ssize_t _publishCount = 0;
	QList<QSharedPointer<Fox>> _clients;
	QMap<uint64_t, uint32_t> _associationCounts;
};

#endif  // FOXSCREAM_H
