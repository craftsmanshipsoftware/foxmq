/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar, Jonathan Bagg

*************************************************************************/
#ifndef FOXMQDEN_H
#define FOXMQDEN_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QByteArray>
#include <QTcpServer>
#include <QSslConfiguration>
#include "fox.h"
#include "foxscream.h"

#if (!defined(QT_STATIC) && !defined(QFOXMQ_STATIC))
#       ifdef QT_BUILD_FOXMQ_LIB
#               define Q_FOXMQ_EXPORT Q_DECL_EXPORT
#       else
#       define Q_FOXMQ_EXPORT Q_DECL_IMPORT
#       endif
#else
#       define Q_FOXMQ_EXPORT
#endif

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QSslSocket)
QT_FORWARD_DECLARE_CLASS(QWebSocket)
class FoxWebSocket;
class FoxSslSocket;

using FoxPermittedPrefixes = std::unordered_set<QByteArray>;

class Q_FOXMQ_EXPORT FoxmqDen : public QTcpServer
{
	Q_OBJECT
public:
	explicit FoxmqDen();
	bool startWebSockets(const QString &name, quint16 port, const QSslConfiguration &config);
	bool startWebSocketsWithoutEncryption(const QString &name, quint16 port);
	bool startSSLSockets(quint16 port, const QSslConfiguration &sslConfig);
	bool startSSLSocketsWithoutEncryption(quint16 port);
	void stopSSLSockets();
	void stopWebSockets();
	QHostAddress getClientsIPAddress(const FoxID &clientID);
	void setPermittedTopics(FoxID client, std::unordered_set<QByteArray> permittedTopics);
	void broadcast(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content);
	void publish(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content);
	void publishBriefly(const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content);
	void updateContentFromChangeRequest(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void removeMessage(const QByteArray &topic);
	uint32_t publishCount(const QByteArray &topic);
	FoxID getID() { return serverID; }
	void sendUnknownTopic(FoxID clientID, QByteArray topic);
	void answerPost(FoxID &clientID, const QByteArray &topic, FoxHead::ContentType contentType, const QByteArray &content);
	void sendError(FoxID clientID, QByteArray topic);
	void ack(FoxID clientID, QByteArray topic);
	void nack(FoxID clientID, QByteArray topic);
	void disconnectClient(uint64_t local);

Q_SIGNALS:
	void closed();
	void contentChange(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content);
	void incomingPost(const QByteArray &topic, uint32_t publishCount, FoxID clientID, FoxHead::ContentType contentType, QByteArray content);
	void newClient(FoxID clientID, bool authenticated);
	void clientDisconnected(FoxID clientID);

private Q_SLOTS:
	void onNewWebSocketConnection();

private:
	void incomingConnection(qintptr socketDescriptor) override;
	void sslSocketReady();
	void setupConnection(QSslSocket *socket);
	void processMessage(FoxHead::Action action, uint32_t publishCount, FoxID clientID, QByteArray topic, FoxHead::ContentType contentType, QByteArray content,
	                    QSharedPointer<Fox> client);
	bool sendCurrentContent(const QByteArray &topic, const Fox &client);
	void sendNoContent(FoxID clientID, FoxHead::Action action, QByteArray topic);
	void buildSubscriptionAssociationsForNewMessage(const QByteArray &topic, QSharedPointer<FoxScream> msg);
	void sendMessageToSubscribedClients(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	QSharedPointer<FoxScream> lookupMessage(QByteArray topic);
	void addSubscriptionAssociations(const QSharedPointer<Fox> &fox, QByteArray topic);
	void removeSubscriptionAssociations(const QSharedPointer<Fox> &fox, QByteArray topic);
	void disconnectClient(QSharedPointer<Fox>);
	Q_INVOKABLE void deleteClient(QSharedPointer<Fox> client);
	QSharedPointer<QWebSocketServer> m_pWebSocketServer;
	QMap<uint64_t, QSharedPointer<Fox>> clients;
	QMap<QByteArray, QSharedPointer<FoxScream>> messages;
	QSslConfiguration sslConfiguration;
	bool sslEncryptionEnabled;
	FoxID nextClientID;
	FoxID serverID;

	friend FoxWebSocket;
	friend FoxSslSocket;
	friend Fox;
};

static inline bool isPartialTopic(const QByteArray &topic, const QByteArray &partialTopic)
{
	if (partialTopic.length() > topic.length()) {
		return false;
	}
	for (int i = 0; i < partialTopic.length(); i++) {
		if ((partialTopic[i] & 0xdf) != (topic[i] & 0xdf)) {
			return false;
		}
	}
	return true;
}

#endif  // FOXMQDEN_H
