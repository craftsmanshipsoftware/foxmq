/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#include "foxscream.h"
#include "fox.h"

FoxScream::FoxScream(const QByteArray &topic) : _head(FoxHead::size(), 0), _topic(topic)
{
	FoxHead header(_head.data());
	header.setTag();
	header.setAction(FoxHead::Publish);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	// header.setId(0);
	header.setTopicLength(topic.size());
}

void FoxScream::addFox(const QSharedPointer<Fox> &client)
{
	if (_clients.contains(client)) {
		return;
	}
	_clients.append(client);
}

void FoxScream::eraseFox(const QSharedPointer<Fox> &client)
{
	_clients.removeOne(client);
}

QByteArray FoxScream::asBinaryMessage() const
{
	return _head + _topic + _content;
}

void FoxScream::publish(const FoxID &id, FoxHead::ContentType contentType, const QByteArray &content)
{
	setContent(id, contentType, content);
	for (auto &client : _clients) {
		client->sendMessage(*this);
	}
}

void FoxScream::setContent(const FoxID &id, FoxHead::ContentType contentType, const QByteArray &content)
{
	FoxHead header(_head.data());
	_publishCount++;
	_content = content;
	header.setPublishCount(_publishCount);
	header.setContentType(contentType);
	header.setContentLength(content.length());
	header.setId(id);
}
