/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOX_H
#define FOX_H
#include <QObject>
#include <QSharedPointer>
#include <QMap>
#include <unordered_set>

#include "../common/foxid.h"

class FoxmqDen;
class FoxScream;
class QHostAddress;

class Fox : public QObject, public QEnableSharedFromThis<Fox>
{
	Q_OBJECT
public:
	Fox(FoxmqDen &foxDen, FoxID id);
	bool authenticated = false;
	bool hasPermission(QByteArray &topic);
	virtual void sendMessage(FoxScream const &message) const = 0;
	virtual void sendData(const QByteArray data) = 0;
	void subscribe(QByteArray &topic);
	void unsubscribe(QByteArray &topic);
	virtual void sendID() = 0;
	virtual QHostAddress peerAddress() = 0;
	void setPermittedTopics(std::unordered_set<QByteArray> permittedTopics);
	QList<QByteArray> subscriptions() { return _subscriptions.keys(); }
	virtual void prepareForDisconnect() = 0;
	FoxID &id() { return _id; }

protected:
	void ask(QByteArray &topic);
	FoxmqDen &_foxDen;
	FoxID _id;
	QMap<QByteArray, int> _subscriptions;
	std::unordered_set<QByteArray> _permittedTopics;
};

#endif  // FOX_H
