/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#ifndef FOXWEBSOCKET_H
#define FOXWEBSOCKET_H
#include "QtWebSockets/qwebsocket.h"
#include "fox.h"
#include "foxmqden.h"
#include <unordered_set>

class FoxWebSocket : public Fox
{
	Q_OBJECT

public:
	FoxWebSocket(QWebSocket *socket, FoxmqDen &foxDen, FoxID id);

	void sendMessage(FoxScream const &message) const override;
	void sendData(const QByteArray data) override;
	void sendID() override;
	QHostAddress peerAddress() override { return socket->peerAddress(); };

private Q_SLOTS:
	void binaryMessageReceived(QByteArray message);
	void disconnected();

private:
	void prepareForDisconnect() override { disconnect(socket, nullptr, nullptr, nullptr); }
	QWebSocket *socket;
};

#endif  // FOXWEBSOCKET_H
