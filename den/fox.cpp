/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#include "fox.h"
#include "foxmqden.h"

Fox::Fox(FoxmqDen &foxDen, FoxID id) : _foxDen(foxDen), _id(id)
{

}

void Fox::subscribe(QByteArray &topic)
{
	if (_subscriptions.contains(topic)) {
		_subscriptions[topic] += 1;
	} else {
		_subscriptions[topic] = 1;
		_foxDen.addSubscriptionAssociations(sharedFromThis(), topic);
	}
}

void Fox::unsubscribe(QByteArray &topic)
{
	if (_subscriptions.contains(topic)) {
		_subscriptions[topic] -= 1;
		if (_subscriptions[topic] == 0) {
			_subscriptions.remove(topic);
			_foxDen.removeSubscriptionAssociations(sharedFromThis(), topic);
		}
	}
}

void Fox::setPermittedTopics(std::unordered_set<QByteArray> permittedTopics)
{
	_permittedTopics = permittedTopics;
}

bool Fox::hasPermission(QByteArray &topic)
{
	for (auto &&permittedTopic : _permittedTopics) {
		if (isPartialTopic(topic, permittedTopic)) {
			return true;
		}
	}
	return false;
}

void Fox::ask(QByteArray &topic)
{
	if (!hasPermission(topic)) {
		_foxDen.sendNoContent(this->id(), FoxHead::Action::ErrorNoPermission, topic);
	} else if (!_foxDen.sendCurrentContent(topic, *this)) {
		_foxDen.sendUnknownTopic(this->id(), topic);
	}
}
