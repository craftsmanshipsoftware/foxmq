/*************************************************************************

Copyright (C) 2023-2024  Craftsmanship Software Inc.
This file is part of FoxMQ

FoxMQ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FoxMQ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FoxMQ.  If not, see <http://www.gnu.org/licenses/>.

Authors: Jeff Muizelaar

*************************************************************************/
#include "foxwebsocket.h"
#include "foxmqden.h"
#include "../common/foxhead.h"

FoxWebSocket::FoxWebSocket(QWebSocket *socket, FoxmqDen &foxDen, FoxID id) : Fox(foxDen, id), socket(socket)
{
	connect(socket, &QWebSocket::binaryMessageReceived, this, &FoxWebSocket::binaryMessageReceived);
	connect(socket, &QWebSocket::disconnected, this, &FoxWebSocket::disconnected);
}

void FoxWebSocket::binaryMessageReceived(QByteArray message)
{
	auto head = FoxHead::create(message);
	if (!head || !head->checkTag()) {
		return;
		// disconnect
	}
	auto action = head->action();
	QByteArray topic = message.mid(head->size(), head->topicLength());
	switch (action) {
		case FoxHead::Action::Subscribe: {
			if (hasPermission(topic)) {
				subscribe(topic);
			}
			break;
		}
		case FoxHead::Action::Unsubscribe: {
			unsubscribe(topic);
			break;
		}
		case FoxHead::Action::Ask: {
			ask(topic);
			break;
		}
		default: {
			QByteArray content = message.mid(head->size() + head->topicLength());
			_foxDen.processMessage(action, head->publishCount(), _id, topic, head->contentType(), content, this->sharedFromThis());
		}
	}
}

void FoxWebSocket::sendMessage(FoxScream const &message) const
{
	socket->sendBinaryMessage(message.asBinaryMessage());
}

void FoxWebSocket::sendData(const QByteArray data)
{
	socket->sendBinaryMessage(data);
}

void FoxWebSocket::sendID()
{
	QByteArray header_data(FoxHead::size(), 0);
	FoxHead header(header_data.data());
	header.setTag();
	header.setAction(FoxHead::Id);
	header.setPublishCount(0);
	header.setContentLength(0);
	header.setContentType(FoxHead::ContentType::Empty);
	header.setId(id());
	header.setTopicLength(0);
	sendData(header_data);
}

void FoxWebSocket::disconnected()
{
	_foxDen.disconnectClient(sharedFromThis());
}
